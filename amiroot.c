/* amiroot.c - A program that exits 0 (true) if euid == 0
 *
 * (C) 2001 by Matthias Andree
 * redistributable under the Lesser GNU Public License v2.1,
 * see the file COPYING.LGPL.
 */

#include <sys/types.h>
#include <unistd.h>

int
main(void)
{
    return geteuid()? 1 : 0;
}
