#!/bin/sh 

set -e

# we need the debconf library
. /usr/share/debconf/confmodule

CONFIG=/etc/news/leafnode/config
SAMPLE=/usr/share/leafnode/config.example
FETCHNEWS=/usr/sbin/fetchnews
DEBIAN_CONFIG=/etc/news/leafnode/debian-config

# If we saved the config file, restore it.
if [ -e /etc/news/leafnode/config.save-upgrade ]; then
	if [ ! -e /etc/news/leafnode/config ]; then
		mv /etc/news/leafnode/config.save-upgrade /etc/news/leafnode/config
	else
		rm -f /etc/news/leafnode/config.save-upgrade
	fi
fi

if [ ! -e ${DEBIAN_CONFIG} ]; then
	cp -p /usr/share/leafnode/debian-config ${DEBIAN_CONFIG}
fi

########################################################################
#
# User configuration
#
########################################################################

if [ "$1" = "configure" ]; then
# Server - keep as many settings we don't know about as possible
    db_get leafnode/server || true
    server=$RET
 
    # Is this a fresh configuration?
    if [ -e $CONFIG ]; then
        cp $CONFIG ${CONFIG}.new
	cp $CONFIG ${CONFIG}.0
    else
        # Debhelper may decide to compress the sample file, so we
        # handle either case.
        if [ -f ${SAMPLE}.gz ] ; then
           gunzip -c ${SAMPLE}.gz > ${CONFIG}.new
        else
           cp $SAMPLE ${CONFIG}.new
        fi

	touch ${CONFIG}
	chown root ${CONFIG}
	chgrp news ${CONFIG}
	chmod 640 ${CONFIG}
    fi

    # Do the substitution (once only - can have multiple server = lines)
awk '/^server.*=/ { if (x == foo) { print "server = '${server}'"; x= "foo"} else print }
!/^server.*=/ { print }' < ${CONFIG}.new > ${CONFIG}

    # Perhaps there was no server line?
    if ! grep "server.*=" ${CONFIG} > /dev/null ; then
        echo server = $server > ${CONFIG}.tmp
        cat ${CONFIG}.tmp ${CONFIG}.new > ${CONFIG}
        rm -f ${CONFIG}.tmp
    fi
    rm -f ${CONFIG}.new
    # Glad that's over...
fi

# Old versions had this line wrong in the config file. Correct.
if grep -q maxcount /etc/news/leafnode/config; then
	cp /etc/news/leafnode/config /etc/news/leafnode/config.new
	sed -e "s/maxcount/maxfetch/" < /etc/news/leafnode/config.new > /etc/news/leafnode/config
	rm -f /etc/news/leafnode/config.new
fi

cd /var/spool/news/message.id
for a in 0 1 2 3 4 5 6 7 8 9 ; do for b in 0 1 2 3 4 5 6 7 8 9 ; do \
install -g news -o news -d ${a}${b}0 ${a}${b}1 ${a}${b}2 ${a}${b}3 ${a}${b}4 \
	${a}${b}5 ${a}${b}6 ${a}${b}7 ${a}${b}8 ${a}${b}9 ; \
done ; done

if [ "$1" = "purge" ]; then
   db_purge || true
fi

#DEBHELPER#

########################################################################
#
# Configuration reprise
#
########################################################################

if [ "$1" = "configure" ]; then
   # Update list of groups if the user decided to do that
   db_get leafnode/update-groups || true
   if [ "$RET" = "true" ]; then
       echo -n "Updating list of active groups... "
       if ${FETCHNEWS} -f >/dev/null ; then
          echo done
       else
          echo failed
       fi
       
       # But don't do it again unless the user asks for it
       db_set leafnode/update-groups false
   fi
fi
