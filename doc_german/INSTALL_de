0.  Seit Version 1.9.17.ma1 verwendet leafnode GNU automake.  Die
    üblichen make-targets existieren.  Für Anpassungen am Quellcode
    wird --enable-maintainer-mode empfohlen.

1.  Seit Version 1.9 verwendet leafnode GNU autoconf, um herauszufinden,
    auf was für einem Rechner es installiert werden soll. Wenn man

        sh ./configure

    ausführt, werden ein rechnerspezifisches Makefile und ein passendes
    config.h erstellt.
    Wenn Sie IPV6 haben und verwenden möchten, rufen sie configure mit
    dem Parameter --with-ipv6 auf.

    Wenn Sie eine bestehende installierte Version von Leafnode updaten,
    konfigurieren Sie die neue Version so, daß sie die alte überschreibt.
    Dies kann mit der Option --prefix von configure eingestellt werden.
    Wenn Leafnode zum Beispiel vorher in /opt installiert war, dann
    geben Sie "./configure --prefix=/opt" ein.  Mit den weiteren Optionen
    --with-spooldir, --sysconfdir und --with-lockfile können die Pfade
    zur News-Hierarchie (per default /var/spool/news), zu den Konfigura-
    tionsdateien (/etc/leafnode, wenn kein --prefix angegeben wurde,
    sonst PREFIX/etc) und zum Lockfile (per default
    leaf.node/lock.file unterhalb des "spooldir") angepaßt werden.

2.  Durch Eingabe von

        make

    (als normaler User) wird der Quelltext compiliert. Es sollten keine
    Fehler auftreten.  Auf Solaris könnten Warnungen der Form "function
    declaration isn't a prototype" auftreten, die getrost ignoriert
    werden können

    Durch Eingabe von

        make check

    werden einige Tests angestoßen.  Bitte melden Sie sich auf der
    Leafnode Mailingliste, falls nicht alle Tests positiv verlaufen
    (PASS).

3.a Legen Sie (als root) einen User "news" an, wenn es einen solchen
    bisher nicht gibt.

3.b Richten Sie (als root) einen Alias im Mailsystem an, das Mails,
    die an den User "news" gerichtet sind, an einen existierenden
    Benutzer weiterleitet, der sich um Leafnode kümmert.

    Wenn Sie qmail verwenden: installieren Sie das fastforward Paket,
    das von den qmail-Seiten erhältlich ist.

    Fügen Sie dazu zu Ihrer Aliases-Datei (/etc/aliases oder
    /etc/mail/aliases) die Zeile "news: joe" hinzu (falls joe der
    für Leafnode zuständige User ist) und geben Sie
        newaliases
    ein.

4.  Die Eingabe von
        make install
    (als root) wird Leafnode installiert.

5.  Wenn Sie von einer alten leafnode-Version vor 1.9.3 (einschließlich
    aller Betaversionen bis incl. 1.9.3b5) updaten, müssen Sie (als
    root) "make update" eingeben.  Dadurch wird das beiliegende Shellscript
    update.sh abgearbeitet.  Es formatiert die groupinfo-Datei um und
    verschiebt einige Dateien in andere Ordner.  Falls dabei etwas
    schiefgehen sollte, finden Sie die alte groupinfo-Datei unter
    /var/spool/news/leaf.node/groupinfo.old . Wenn "make update" fehlerfrei
    funktioniert hat, kann diese Datei gelöscht werden; sie wird nicht mehr
    benötigt.

6.  Passen Sie die $(sysconfdir)/config mit einem Editor an Ihre
    Verhältnisse an ($(sysconfdir) ist gewöhnlich /etc/leafnode). Die
    Syntax der Datei ist in config.example und leafnode(8) dokumentiert.

6a. Es ist unbedingt notwendig, beim Wert für "server" den eigenen
    Upstream-Newsserver (in der Regel ist das der Newsserver Ihres
    Providers) einzutragen.

6b. In die Environment-Variable $NNTPSERVER oder die Datei
    /etc/nntpserver muß der Name des eigenen Rechners eingetragen werden,
    damit Newsreader auch wirklich auf Leafnode zugreifen und nicht auf den
    Upstream-Server.  Wenn Sie die eingehenden News filtern möchten, finden
    Sie Einzelheiten dazu weiter unten unter FILTER-DATEI.

6c. Setzen Sie "initialfetch = 200" oder ähnlich in der Konfigurations-
    datei, um zu verhindern, daß neue Gruppen mit hohem Volumen die
    Platten füllen.

7.  Falls Ihr System keinen "fully qualified domain name" (FQDN) hat,
    besorgen Sie sich bitte einen von Ihrem Newsprovider (oder einer der
    unten angegeben Stellen) und tragen Sie ihn in Ihre /etc/hosts Datei
    ein.   Beispiel:  Falls Ihr FQDN debian.example.com ist und in
    /etc/hosts eine Zeile der Form

    192.168.0.1 debian

    steht, ändern Sie diese bitte in

    192.168.0.1 debian.example.com debian

    Zum Thema FQDN und Message-IDs (und Möglichkeiten, FQDNs zu bekommen),
    gibt es weitere Informationen in der Datei README-FQDN sowie hier:

    http://www.hanau.net/faq_message-id_ueberschreiben.php
    http://fqdn.th-h.de/
    http://www.rumil.de/faq/kommentar.html

8.  Richten Sie (als root) für den User "news" einen cron-Job ein, der
    jede Nacht (oder wenigstens einmal pro Woche) texpire ausführt. Meine
    entsprechende crontab-Zeile sieht folgendermaßen aus (texpire läuft
    jede Nacht um 4:00):

0 4 * * * /usr/local/sbin/texpire

    Um die Crontab-Datei zu editieren, habe ich als root "crontab -u news -e"
    eingegeben und obige Zeile eingefügt. Wenn man den dritten "*" durch
    eine "1" ersetzt,

0 4 * * 1 /usr/local/sbin/texpire

    dann wird texpire nur jeden Montag um 4:00 ausgeführt. Weitere
    Informationen über das Generieren von Crontab-Zeilen finden Sie
    in der manpage zu crontab(5).

9.  Stellen Sie sicher, daß fetchnews zu einer geeigneten Zeit
    aufgerufen wird. Wenn Sie über eine Standleitung verfügen, ist es
    am besten, fetchnews von cron ausführen zu lassen (wieder als User
    "news"); anderenfalls muß fetchnews ausgeführt werden, während
    Sie online sind. Wenn fetchnews vom User "root" gestartet wird,
    wird es zum User "news" wechseln. Falls Sie PPP benutzen, können
    Sie fetchnews aus /etc/ppp/ip-up starten. 

10. (Als root) Fügen Sie zu /etc/hosts.deny die Zeile

    leafnode: ALL

    hinzu.  Fügen Sie zu /etc/hosts.allow die Zeile

    leafnode: 127.0.0.1

    hinzu, um Zugriff vom lokalen Rechner zu erlauben.  Falls Sie Zugriff
    vom LAN ebenfalls erlauben möchten, könnte die Zeile folgendermaßen
    aussehen (bitte die Netznummern anpassen):

    leafnode: 127.0.0.1 192.168.0.0/255.255.255.0

    Damit schützen Sie ihren Server vor Mißbrauch durch andere Leute.
    Weitere Informationen gibt es in den manpages zu hosts_access(5)
    und hosts_options(5).

11. Es hängt vom lokalen System ab, wie leafnode letztlich gestartet
    wird.

    WARNUNG:  Unabhängig von der Methode, mit der der Zugriff beschränkt
    wird, verwenden Sie keine Namenbasierte (DNS) Zugriffsregeln, sondern
    IP-basierte (wie 1.2.3.4).  Verwenden Sie keine Namen, die zu
    dynamischen IP-Adressen auflösen oder auf dynamische IP-Adressen
    zeigen, zur Zugangskontrolle.

    Die Alternativen sind (wählen Sie nur eine):

    a) Ursprünglich wurde die Funktionalität, Netzwerk-Server zu starten,
    lediglich durch inetd bereitgestellt, auf HP-UX 10, Solaris 8, *BSD
    und einigen anderen ist das immer noch der Fall; siehe dazu Abschnitt
    11a.  Da jedoch die meisten inetd-Implementationen Designschwächen
    aufweisen, ist dies nur für FreeBSD empfehlenswert.  Die Konfiguration
    von inetd erfolgt üblicherweise in der Datei /etc/inetd.conf oder
    /etc/inet/inetd.conf.

    b) Mit xinetd wurde diese Funktionalität mit verbessertem Design
    neu implementiert; xinetd wird in Red Hat und SUSE Linux eingesetzt.
    Siehe dazu Abschnitt 11b.

11a.NUR FALLS SIE INETD BENUTZEN: (die meisten aktuellen Distributionen
    verwenden xinetd, siehe 11b. unten)

    Editieren Sie (als root) /etc/inetd.conf so, daß leafnode bei
    ankommenden NNTP-Verbindungen gestartet wird. Bei mir sieht die
    entsprechende Zeile folgendermaßen aus:

    nntp stream  tcp nowait news /usr/sbin/tcpd /usr/local/sbin/leafnode

    Dadurch wird leafnode für alle Verbindungen auf dem nntp-Port
    gestartet, wenn der Inhalt der Dateien /etc/hosts.allow und
    /etc/hosts.deny nicht dagegen sprechen.  Wenn auf Ihrem System kein
    /usr/sbin/tcpd installiert ist, besorgen Sie sich bitte das Paket
    tcp_wrappers und installieren es.   Benutzung von Leafnode ohne
    tcpd wird nicht unterstützt und ist aufgrund der Gefahr durch
    Mißbrauch dringendst abzuraten!

    Wenn Sie /etc/inetd.conf editiert haben, müssen Sie dem inetd diese
    Veränderungen mitteilen. Der inetd wertet sein Konfigurationsfile
    erneut aus, wenn Sie ihm ein HANGUP-Signal schicken. Dies tun Sie,
    indem Sie als root eingeben:

    kill -HUP `cat /var/run/inetd.pid`

    Weiter mit Abschnitt 12.

11b.FALLS SIE XINETD benutzen:

    xinetd Versionen vor 2.3.3 werden nicht unterstützt.  Es ist
    unbekannt, ob Leafnode mit älteren Versionen funktioniert.

    Für Informationen zu xinetd konsultieren Sie bitte die manpages
    zu xinetd und xinetd.conf.

    a. Überprüfen Sie, ob /etc/xinetd.conf eine Zeile der Form
       "includedir /etc/xinetd.d" enthält.  Falls ja, so führen Sie
       bitte die folgenden Änderungen in /etc/xinetd.d/leafnode durch,
       falls nicht, so fügen Sie die Änderungen bitte an die Datei
       /etc/xinetd.conf an.

    b. Editieren Sie die gewählte Datei, indem Sie folgenden Eintrag
       hinzufügen:

	service nntp
	{
	    flags           = NAMEINARGS NOLIBWRAP
	    socket_type     = stream
	    protocol        = tcp
	    wait            = no
	    user            = news
	    server          = /usr/sbin/tcpd
	    server_args     = /usr/local/sbin/leafnode
	    instances       = 7
	    per_source      = 3
	}

    Dieser Eintrag erlaubt maximal 7 Verbindungen zu Leafnode gleich-
    zeitig, davon maximal 3 vom selben Rechner.  Sie können diese Werte
    anpassen.

    Um xinetd die Änderungen mitzuteilen, senden Sie ein USR2 Signal.
    Finden Sie dazu die PID des laufenden xinetd mit
        ps ax | egrep '[x]inetd'
    auf Linux oder *BSD, oder mit
        ps -ef | egrep '[x]inetd'
    auf SysV-Systemen (Solaris) heraus.  Dann geben Sie
        kill -s USR2 12345
    (als root) ein, wobei Sie 12345 mit der PID ersetzen.

    Weiter mit Abschnitt 12.

12. Führen Sie fetchnews aus. Der erste Aufruf von fetchnews wird
    einige Zeit dauern,  weil fetchnews eine Liste aller auf dem
    Upstream-Server vorhanden Gruppen einliest. Mit einem 28.8 Modem
    kann dies bis zu 60 Minuten dauern (abhängig davon, wie viele
    Gruppen Ihr Provider anbietet). Um zu sehen, was fetchnews gerade
    tut, können Sie es mit der Option -vvv starten. 
    Wenn Sie leafnode von einer Version vor 1.6 updaten, rufen Sie
    fetchnews bitte mit dem Parameter -f auf, da sich das Format der
    groupinfo-Datei geändert hat. 
   
13. Lesen Sie (als normaler Benutzer) News mit einem NNTP-fähigen
    Newsreader (in $NNTPSERVER oder /etc/nntpserver muß dazu Ihr eigener
    Rechner eingetragen sein).  Wählen Sie die Gruppen aus, die Sie in
    Zukunft lesen möchten. Diese sind zunächst einmal leer, abgesehen von
    einem Platzhalterartikel.  Bei einigen Newsreadern ist es notwendig,
    daß Sie diesen Artikel in jeder gewünschten Gruppe lesen, damit die
    Gruppe in Zukunft abonniert wird.

    Danach sollten Sie für jede selektierte Gruppe eine leere Datei
    in /var/spool/news/interesting.groups/ vorfinden.

14. Lassen Sie fetchnews noch einmal laufen; nun sollten für alle
    ausgewählten Gruppen Artikel geholt werden. Es empfiehlt sich
    dabei, initialfetch auf einen sinnvollen Wert zu setzen, da
    leafnode sonst alle Artikel holt, die upstream vorhanden sind (und
    das kann dauern).  

15. Falls Sie sich von außerhalb Ihres LAN (außerhalb der Subnetze der
    lokalen Interfaces) zu Leafnode verbinden möchten, lesen Sie bitte
    die Dateien README_de und config.example für die seit Version 1.9.23
    notwendigen Einstellungen.

Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>
Matthias Andree <matthias.andree@gmx.de>
Deutsche Übersetzung: Alexander Stielau <aleks@zedat.fu-berlin.de>
                       Alexander Reinwarth <a.reinwarth@gmx.de>
		       Klaus Fischer <fischer@reutlingen.netsurf.de>
                       Ralf Wildenhues <Ralf.Wildenhues@gmx.de>
