/* (C) 2001 - 2010 by Matthias Andree */

#include "leafnode.h"
#include <unistd.h>

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

#include "ln_log.h"

int
log_unlink(const char *f, int ignore_enoent)
{
    int r = unlink(f);
    if (r < 0 && errno == ENOENT && ignore_enoent)
	r = 0;
    if (r < 0)
	ln_log(LNLOG_SERR, LNLOG_CTOP, "cannot unlink %s: %m", f);
    else {
	if (debugmode)
	    ln_log(LNLOG_SDEBUG, LNLOG_CTOP, "unlinked %s", f);
    }
    return r;
}
