/*
mgetaline.c -- getaline with timeout, split from the NNTP server

Written by Arnt Gulbrandsen <agulbra@troll.no> and copyright 1995
Troll Tech AS, Postboks 6133 Etterstad, 0602 Oslo, Norway, fax +47
22646949.
Modified by Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>
and Randolf Skerka <Randolf.Skerka@gmx.de>.
Copyright of the modifications 1997.
Modified by Kent Robotti <robotti@erols.com>. Copyright of the
modifications 1998.
Modified by Markus Enzenberger <enz@cip.physik.uni-muenchen.de>.
Copyright of the modifications 1998.
Modified by Cornelius Krasel <krasel@wpxx02.toxi.uni-wuerzburg.de>
and Kazushi (Jam) Marukawa <jam@pobox.com>.
Copyright of the modifications 1998, 1999.
Modified by Ralf Wildenhues <ralf.wildenhues@gmx.de>
Copyright of the modifications 2002.
Modified by Richard van der Hoff <richard@rvanderhoff.org.uk>
Copyright of the modifications 2002.
Modified by Matthias Andree <matthias.andree@gmx.de>
Copyright of the modifications 2000 - 2010.

See file COPYING for restrictions on the use of this software.
*/

#include "leafnode.h"
#include "mysigact.h"

#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <setjmp.h>
#include <stdio.h>

static unsigned int timeout_secs;

sigjmp_buf timeout;

void timer(int sig)
{
    siglongjmp(timeout, 1);
    exit(sig);			/* not reached */
}

/*
 * call getaline with config specified timeout
 */
/*@dependent@*/ char *
mgetaline(FILE * f)
{
    char *l;

    if (sigsetjmp(timeout,1)) {
	if (debug)
	    syslog(LOG_DEBUG, "< (ERROR: timeout - no response in %u s)", timeout_secs);
	return NULL;
    }
    mysigact(SIGALRM, SA_RESETHAND, timer, 0);
    (void)alarm(timeout_secs);
    l = getaline(f);
    (void)alarm(0U);
    mysigact(SIGALRM, 0, SIG_DFL, 0);
    return l;
}

void
mgetaline_settimeout(unsigned int t) {
    timeout_secs = t;
}
